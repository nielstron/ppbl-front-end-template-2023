import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module203.json";
import Docs2032 from "@/src/components/course-modules/203/Docs2032.mdx";

export default function Lesson2032() {
  const slug = "2032";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={203} sltId="203.2" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2032 />
    </LessonLayout>
  );
}
